#!/usr/bin/env bash
docker kill somerabbit
docker container rm somerabbit
docker run -d --hostname myrabbit --name somerabbit -p 5672:5672 -p 15672:15672 rabbitmq:3-management

docker-compose down --remove-orphans
docker-compose build
docker-compose up -d
#sleep 45
until $(curl --output /dev/null --silent --head --fail http://c.mkir.dk:8080/tokenmanager); do
    printf '.'
    sleep 2
done
until $(curl --output /dev/null --silent --head --fail http://c.mkir.dk:8181/payment); do
    printf '.'
    sleep 2
done
cd ../merchant-app/merchantApp
mvn clean test
cd ../../customer-app
mvn clean test